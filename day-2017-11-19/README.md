# python学习的同学，下一周任务：
1. 学习linux操作系统下CPU的运行管理机制。---编写python小程序，获取本机的核数。
2. 学习linux系统的进程、子进程、线程，区分进程与线程的差别。---编写python小程序，并行的访问百度。

# WEB前端的同学
1. 学习linux操作系统下CPU的运行管理机制。时间富余，了解系统的进程、子进程、线程，区分进程与线程的差别
2. 上周期望写一个nodejs的demo，这周在这个基础上加一点，期望显示的不是Hello world，输出本机cpu核数，如"CPU core is <cpu cores>"