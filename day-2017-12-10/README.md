## 类定义、继承
1. 学习python类的定义，注意关注__init__, __call__等方法。
2. 类继承
3. Demo：
  - 要求定义Ping类，包含default_ip, default_cout属性及定义execute方法完成ping 指定的域名或ip，测试ip为：114.114.114.114， www.baidu.com   (初始化Ping类时传入实际ip、count)。
  - 要求定义Ifconfig类，包含default_interface属性及定义show方法，完成打印指定网卡信息。
  - 定义NetTools类，继承Ping、Ifconfig，定义check方法，显示本机mac地址、ip地址、子网掩码、网关信息，并检测是否联通外网，如果联通，显示Connected WLAN，否则显示Do not connect WLAN。

## git学习
1. 学会使用git向https://gitee.com/HuShi-ZheTeng/Learning-cloud-computing提交文件。
2. 学会git commit及git push、git pull、git fetch指令的用法及意义。