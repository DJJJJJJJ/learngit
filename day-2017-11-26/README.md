# 本周任务
* 根据下面的问题，去了解CPU，进程和线程：
  - CPU是怎么执行计算，并处理程序的？
  - CPU执行了哪些计算？什么是CPU指令集
  - 进程是怎么使用CPU的？什么是内核态，什么是用户态
  - 什么是是进城锁？
  - 什么是线程锁？进程和线程间有什么样的关联？
每人整理文档，捋清自己的理解，下周咱们相互看下。**注：完全弄清CPU的工作原理，你们现在可能会比较吃力，但是要了解它的工作流程，干什么用的，怎么处理程序的。**

* 编写一个程序，要求能够并行ping www.baidu.com，gitee.com，biying.com，需要看到ping的结果，执行ctr+c之后，退出进程

# 参考文档

1. 处理器体系结构 http://blog.csdn.net/yang_yulei/article/details/22529437
2. CPU的内部架构和工作原理 http://blog.chinaunix.net/uid-23069658-id-3563960.html
3. 进程与线程的简单解释 http://www.ruanyifeng.com/blog/2013/04/processes_and_threads.html
4. 进程与线程 https://www.cnblogs.com/lmule/archive/2010/08/18/1802774.html