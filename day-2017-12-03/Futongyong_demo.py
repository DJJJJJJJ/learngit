def get_res(x, y, gcd=False, lcm=False):
    if gcd:
        print "The greatest common divisor is:",get_gcd(x,y)
        return get_gcd(x, y)
    if lcm:
        print "The least common multiple is:"
        return get_lcm(x, y)


def get_gcd(x, y):
    """Greatest common divisor."""
    # greatest common divisor
    j = 2;
    if x < y:
        i = x
    else:
        i = y
    while i > j:
        if x % j == 0 and y % j == 0:
            break
        else:
            j = j+1
    if i == j:
        j = 1;
    return j




#least common multiple
def get_lcm(x, y):
    a = get_gcd(x, y)
    a = x * y / a
    return a

    



if __name__=="__main__":
    print
    x = int (raw_input("please enter the first number:"))
    y = int (raw_input("please enter the second number:"))
    print get_res(x, y, 0, lcm=1)
    print get_res(x,y,1,lcm=0)
