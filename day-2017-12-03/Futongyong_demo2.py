import subprocess
import shlex

cmd = " ping -c 1 www.baidu.com"
args = shlex.split(cmd)

try:
    subprocess.check_call(args,stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    print "baidu server is up!"
except subprocess.CalledProcessError:
    print "Failed to get ping."
