# 本周任务
* 学习python函数的使用
  - 学习python函数、函数参数传递
  - 函数默认参数定义
  - 函数可变列表的定义
  - 测试：编写demo完成：
    - 定义一个函数完成ping 指定网址域名，如果传入为空，默认为ping 114.114.114.114。
    - 定义一个函数get_res完成指定的计算：如果参数列表中包含gcd，且gcd=True，则返回传入整数的最大公约数，如果参数列表中包含lcm，且lcm=True，则返回传入整数的最小公倍数，如果gcd和lcm都为True，则返回传入整数的最大公约数，并且要求分别定义函数get_gcd和get_lcm完成获取整数的最大公约数和最小公倍数。
* 学习git的使用
  - 学习``git clone``，``git push``，``git pull``，``git fetch``，``git rebase``等命令的使用
  - 了解git管理的过程
  - 测试：使用git将自己的demo提交到day-2017-12-03文件夹下，命名为XXX_demo.py（XXX为自己名字的全拼，首字母大写）
* 选举下周末一起交流的新概念

# 参考文档
* python函数基础：http://www.cnblogs.com/livingintruth/p/3263972.html
* 比较详细的python函数学习：http://blog.csdn.net/kangqiao182/article/details/8624460
* git 教程：https://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000
* git基本操作：http://www.runoob.com/git/git-basic-operations.html