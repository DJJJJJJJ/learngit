# Author: Cao Qianchang<olivercqc@foxmail.com>

"""This is a demo.

When input odd number, get sum from 1 to it,
when input even number, divid it until get a odd number,
otherwise, raise error.
"""


list = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
# Input a string
string = input('Please input something:')
for i in range(len(string)):
    if (string[i] not in list) or (len(string) >= 10):
        print('Error:You did not enter a number or the number is too big')
        break
    else:
        num = int(string)
        while num % 2 == 0:
            print('This is a even number and it needs to be processed')
            num = num / 2
            print("The number after processing is {}".format(num))
        sum = (1 + num) * num * 0.5
        print('the accumulate of number is {}'.format(sum))
        break
