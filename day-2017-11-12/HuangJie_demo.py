
"""
输入若干字符，判断是否是数字，且为>0的整数，enter后，
如果该数字是奇数，输出该1到该数字的累加和，如果是偶数，
除以2，查看结果是否是奇数，如果是偶数继续除以2，直到结果是奇数。
如果输入字符串不是数字，且长度>10，提示："Not number or number is bigger than 1000000000"
"""

str = input("Please enter some positive integers:")

#如果为整数
if(str.isdigit()):
	num = int(str)
	#如果为正整数
	if(num > 0):
		#为奇数
		if(num % 2 != 0):
			sum = 0
			while(num > 0):
				sum += num
				num -= 1
			print("1 到", str, "的累加和为：", sum)
		#为偶数
		else:
			while(num % 2 == 0):
				num /= 2
				print("num =", num)
	#非正整数
	else:
		print("输入有误！")
#如果不全为数字或不为数字或不为整数
elif(len(str) > 10):
	print("Not number or number is bigger than 1000000000")
else:
	print("输入有误！")

print("谢谢使用，再见！")