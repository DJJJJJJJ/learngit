# Author: Fu Tongyong<email.xx.com>

"""This is a demo. 

When input odd number, get sum from 1 to it,
when input even number, divid it until get a odd number,
otherwise, raise error.
"""


try:
    # Input a value.
    number = int(raw_input( "Please enter a number:"))
    # Logical code.
    if number <= 0:
         print "The number is smaller than 0 or bigger than 1000000000"
    elif number > 10000000000:
         print "The number is smaller than 0 or bigger than 1000000000"
    elif number % 2 == 1:
         print number, "is an odd number"
         add = (1 + number) * number * 0.5
         print "Accumulate and equal from 1 to ", number, "is ", add
    else:
         print number, " is an even number"
         while number % 2 == 0:
               number /= 2
         print "The number is", number, "after processing"
except ValueError:
    # Exception.
    print "Error:You did not enter a number"
